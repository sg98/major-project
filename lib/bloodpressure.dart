import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:nice_button/nice_button.dart';
import 'BPForm.dart';

Color firstColor = Colors.red;
class BloodPressure extends StatelessWidget {
     @override
     Widget build(BuildContext context) {
          return new MaterialApp(
               title: "Dashboard",
               home: new Dashboard(title: 'Dashboard'),
          );
     }
}

class Dashboard extends StatefulWidget {
     Dashboard({Key key, this.title}) : super(key: key);

     final String title;

     @override
     DashBoard createState() => new DashBoard();
}

class DashBoard extends State<Dashboard> {

     var sys = "0" ;
     var dys = "0" ;

     String msg = "nothing in here at the moment";

     getSys() async {
          final prefs = await SharedPreferences.getInstance();
          String sys1 = prefs.getString("sys");
          print('$sys1 was read from file');
          setState(() {
            this.sys = sys1;
          });
     }

     getDys() async {
          final prefs = await SharedPreferences.getInstance();
          String dys1 = prefs.getString("dys");
          print('$dys1 was read from file');

          setState(() {
            this.dys = dys1;
          });
     }
     setWarn() async{
          final prefs = await SharedPreferences.getInstance();
          String dys1 = prefs.getString("dys");
          String sys1 = prefs.getString("sys");
          print('we now at the set warning function');

          int s = int.parse(sys1);
          int d = int.parse(dys1);
          print('$s and $d');

          if (s < 120 && d < 80){
               print("blood pressure is ok");
               msg = "Your blood pressure is ok";
          }
          else if (s >= 120 && s <= 129 && d >= 80 && d <= 89 ){
               print("Warning your blood pressure is elevated");
               setState(() {
                    this.msg = "Warning your blood pressure is elevated";
               });
          }
          else if (s >= 130 && s <= 139 && d >= 80 && d <= 89){
               print("Warning you have stage one Hypertension\nYou may want to consider seeking medical\n attention if you have not already done so.");
               setState(() {
                    this.msg = "Warning you have stage one Hypertension You may want to consider seeking medical attention if you have not already done so.";
               });
          }
          else if (s >= 140 && s <= 179 && d >= 90 && d <= 119){
               print("Warning you have stage two Hypertension\nYou need to consider seeking medical\n attention if you have not already done so.");
               setState(() {
                    this.msg = "Warning you have stage two Hypertension You need to consider seeking medical attention if you have not already done so.";
               });
          }
          else if (s >= 180 || d >=120){
               print("You have an Hypertensive Crisis\t\tGET MEDICAL HELP NOW");
               setState(() {
                    this.msg = "You have an Hypertensive Crisis GET MEDICAL HELP IMMEDIATELY";
               });
          }else{
               this.msg = "Your Blood Pressure is safe";
          }
     }

     @override
     void initState(){
          getSys();
          getDys();
          setWarn();
          super.initState();
     }

     Widget build(BuildContext context) {
          return Scaffold(
               appBar: AppBar(
                    title: Text("Your Blood Pressure",
                        style: TextStyle(color: Colors.white)
                    ),
                    backgroundColor: Colors.red,
               ),

          body: DecoratedBox(
                  decoration: BoxDecoration(
                       image: DecorationImage(image: AssetImage("assets/background11.jpg"), fit: BoxFit.cover),
                  ),
               child: Column(
                   crossAxisAlignment: CrossAxisAlignment.center,
                   children: <Widget>[
                        new Padding(padding: EdgeInsets.all(25.0)),
                        Row(
                             children: [
                                  new Padding( padding: EdgeInsets.all(15.0)),
                                  new Align(alignment: Alignment.center),
                                  new SizedBox(
                                       height: 200.0,


                                            child: Text('\n\t\t\t Systolic: $sys \n\n\t\t\t Diastolic: $dys\n\n\n',
                                                 style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30, color: Colors.red[900]),
                                            ),
                                  )
                             ],
                        ),
                        new Padding( padding: EdgeInsets.all(5.0)),
                        NiceButton(
                             elevation: 10,
                             radius: 25,
                             width: 275,
                             text:'Update Blood Pressure',
                             onPressed: () {
                                  Navigator.push(
                                       context,
                                       MaterialPageRoute(builder: (context) => BPForm()),
                                  );
                             },
                             background: firstColor,
                        ),
                        new Padding( padding: EdgeInsets.all(25.0)),
                        Row(
                             children: [
                                  new Padding( padding: EdgeInsets.all(5.0)),
                                  new Flexible(
                                       child: Text('\n\t\t$msg',
                                            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20, color: Colors.red[900]),
                                            textAlign: TextAlign.center,
                                       ),
                                  )
                             ],
                        ),
                   ]
               ),
          )
          );
     }
}