import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:nice_button/nice_button.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'main.dart';

class Exercise extends StatelessWidget {
     @override
     Widget build(BuildContext context) {
          return new MaterialApp(
               title: "Blood Pressure Analysis",
               home: new ExPage(title: 'Daily Exercise Goals'),
          );
     }
}

class ExPage extends StatefulWidget {
     ExPage({Key key, this.title}) : super(key: key);

     final String title;

     @override
     ExerciseR createState() => new ExerciseR();
}

class ExerciseR extends State<ExPage> {
     String excer = "";

     setmsg() async{

          final prefs = await SharedPreferences.getInstance();
          String dys1 = prefs.getString("dys");
          String sys1 = prefs.getString("sys");

          int s = int.parse(sys1);
          int d = int.parse(dys1);
          print('$s and $d');

          if (s < 120 && d < 80){
               print("blood pressure is ok");
               setState(() {
                    this.excer = " \t\t\nNot Hypertensive\n"
                        "\n•	Control your salt intake and reduce your intake of greasy food. Be sure to exercise 3 times per week for at least 30 minutes. Also avoid smoking and excessive alcohol consumption and do a yearly blood pressure checkup";
               });
          }
          else if (s >= 120 && s <= 129 && d >= 80 && d <= 89 ){
               print("Warning your blood pressure is elevated");
               setState(() {
                    this.excer = "\t\t\nPre-hypertension\n"
                        "\n•	Control your salt intake and reduce your intake of greasy food. Be sure to exercise 3 times per week for at least 30 minutes. Also avoid smoking and excessive alcohol consumption and go for a blood pressure checkup every 6 months.";
               });
          }
          else if (s >= 130 && s <= 139 && d >= 80 && d <= 89){
               print("Warning you have stage one Hypertension\nYou may want to consider seeking medical\n attention if you have not already done so.");
               setState(() {
                    this.excer = "\t\t\nStage 1 hypertension\n"
                        "\n•	Start a low salt, low fatty food diet, exercise 3 times per week, no smoking and no alcohol. Ensure medications are taken. Do not intentionally skip clinic visits, that is, keep ALL doctors’ appointments. Do recommended blood tests 1-2 times per year"
                        "\n•	ECG once per year"
                        "\n•	Get eyes tested once per year"
                        "\n•	Get a blood pressure machine and monitor blood pressure or get annual medical checkups.";
               });
          }
          else if (s >= 140 && s <= 179 && d >= 90 && d <= 119){
               print("Warning you have stage two Hypertension\nYou need to consider seeking medical attention if you have not already done so.");
               setState(() {
                    this.excer = "\t\t\nStage 2 hypertension \n"
                        "\n•	Comply with medication, eat more vegetables, do more exercise, get adequate sleep, get checkups at least every 6 months";
               });
          }
          else if (s >= 180 && d >=120){
               print("You have an Hypertensive Crisis\t\tGET MEDICAL HELP NOW");
               setState(() {
                    this.excer =  "\t\t\nStage 3 hypertension\n"
                        "\n•	Check yourself into a clinic or hospital immediately. You are at serious risk."
                        "\n•	If left untreated, your risk of dying from diseases increases and you may die within 10 months."
                        "\n•	Follow your prescribed blood pressure medication strictly."
                        "\n•	Get checkups at least every 3 to 4 months.";
               });
          }else{
               setState(() {
                    this.excer = " \t\t\nNot Hypertensive\n"
                        "\n•	Control your salt intake and reduce your intake of greasy food. Be sure to exercise 3 times per week for at least 30 minutes. Also avoid smoking and excessive alcohol consumption and do a yearly blood pressure checkup";
               });
          }
     }
     @override
     void initState(){
          setmsg();
          super.initState();
     }

     Widget build(BuildContext context) {
          return Scaffold(
              appBar: AppBar(
                   title: Text("Your Blood Pressure",
                       style: TextStyle(color: Colors.white)
                   ),
                   backgroundColor: Colors.red,
              ),

              body: DecoratedBox(
                   decoration: BoxDecoration(
                        image: DecorationImage(image: AssetImage("assets/background11.jpg"), fit: BoxFit.cover),
                   ),
                       child: Column(
                           crossAxisAlignment: CrossAxisAlignment.center,
                           children: <Widget>[
                                new Padding(padding: EdgeInsets.all(5.0)),
                                Row(
                                     children: [
                                          new Padding( padding: EdgeInsets.all(5.0)),
                                          new Flexible(
                                               child: Text("Based on your Blood Pressure readings you should take the following actions (based on recommendations from Medical Professionals)\n\n $excer",
                                                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20, color: Colors.red[900]),
                                                    textAlign: TextAlign.left,
                                               ),
                                          )
                                     ],
                                ),
                              ]
                       ),
              )
          );
     }
}