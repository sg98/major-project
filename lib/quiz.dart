import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:nice_button/nice_button.dart';

Color firstColor = Colors.red;


class Quiz extends StatelessWidget {
     @override
     Widget build(BuildContext context) {
          return Scaffold(
              appBar: AppBar(
                   title: Text('Quiz',
                        style: TextStyle(color : Colors.white),
                   ),
                   centerTitle: true,
                   backgroundColor: Colors.red,
              ),
              body:
              DecoratedBox(
                  decoration: BoxDecoration(
                       image: DecorationImage(image: AssetImage("assets/background11.jpg"), fit: BoxFit.cover),
                  ),
                  child: Column(children: <Widget>[
                       new Padding(padding: EdgeInsets.all(65.0)),
                       Row(
                           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                           children: [
                                NiceButton(
                                     elevation: 10,
                                     text: 'Quiz 1',
                                     width: 150,
                                     radius: 52,
                                     background: firstColor,
                                     onPressed: (){
                                          launch('https://docs.google.com/forms/d/13hLSUX3pov40_5p8W3GqAMc4Ew6zFJARlITBdqFRTK4/viewform?edit_requested=true');
                                     },
                                ),
                           ]
                       ),
                       new Padding(padding: EdgeInsets.all(45.0)),
                       Row(//ROW 2
                           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                           children: [
                                NiceButton(
                                     elevation: 10,
                                     text: 'Quiz 2',
                                     width: 150,
                                     radius: 52,
                                     background: firstColor,
                                     onPressed: () async{
                                          const url = 'https://docs.google.com/forms/d/13hLSUX3pov40_5p8W3GqAMc4Ew6zFJARlITBdqFRTK4/viewform?edit_requested=true';

                                          if (await canLaunch(url)) {
                                          await launch(url, forceSafariVC: false);
                                          } else {
                                          throw 'Could not launch $url';
                                          }
                                     },
                                ),
                           ]
                       ),
                       new Padding(padding: EdgeInsets.all(35.0)),
                  ]
                  )
              )
          );
     }
}