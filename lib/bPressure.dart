import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BPressure extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Blood Pressure Readings Chart",
        style: TextStyle(color: Colors.white)
        ),
        backgroundColor: Colors.red,
      ),
      body: SingleChildScrollView(
            child: Column(
              children: [
                Image(
                  image: AssetImage('assets/bloodpressure.jpg'),
                  fit: BoxFit.fitWidth,
                ),
              ],
            ),
          ),
    );
  }
}