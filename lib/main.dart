import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:myapp/lessons.dart';
import 'package:nice_button/nice_button.dart';
import 'package:url_launcher/url_launcher.dart';
import 'bPressure.dart';
import 'bloodpressure.dart';
import 'excer.dart';

void main() => runApp(MaterialApp(
  home: Home(),
));

Color firstColor = Colors.red;

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Hypertension Health App',
            style: TextStyle(color : Colors.white),
          ),

          centerTitle: true,
          backgroundColor: Colors.red,
        ),
        body:
        DecoratedBox(
             decoration: BoxDecoration(
                  image: DecorationImage(image: AssetImage("assets/background11.jpg"), fit: BoxFit.cover),
             ),
             child: Column(children: <Widget>[
               new Padding(padding: EdgeInsets.all(35.0)),
               Row(
                   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                   children: [
                         NiceButton(
                             elevation: 10,
                             text: 'Your BP',
                             width: 280,
                             radius: 52,
                             background: firstColor,
                             onPressed: (){
                                  Navigator.push(
                                       context,
                                       MaterialPageRoute(builder: (context) => Dashboard()),
                                  );
                             },
                        ),

                   ]
               ),
               new Padding(padding: EdgeInsets.all(25.0)),
               Row(//ROW 2
                   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                   children: [
                        NiceButton(
                             elevation: 10,
                             text: 'BP Recommendations',
                             width: 280,
                             radius: 25,
                             background: firstColor,
                             onPressed: (){
                                  Navigator.push(
                                       context,
                                       MaterialPageRoute(builder: (context) => ExPage()),
                                  );
                             },
                        )
                   ]
               ),
                  new Padding(padding: EdgeInsets.all(25.0)),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                           NiceButton(
                                elevation: 10,
                                text: 'Lessons',
                                width: 280,
                                radius: 52,
                                background: firstColor,
                                onPressed: (){
                                     Navigator.push(
                                          context,
                                          MaterialPageRoute(builder: (context) => Lesson()),
                                     );
                                },
                           ),
                      ]
                  ),
                  new Padding(padding: EdgeInsets.all(25.0)),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                       NiceButton(
                       elevation: 10,
                       width: 280,
                       text: 'BP Chart',
                       radius: 52,
                       background: firstColor,
                       onPressed: (){
                            Navigator.push(
                                 context,
                                 MaterialPageRoute(builder: (context) => BPressure()),
                            );
                       },
                  )
        ]
                  ),
                  new Padding(padding: EdgeInsets.all(35.0)),

                    ]
                  )
             )
    );
  }
}


