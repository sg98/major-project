import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:myapp/bloodpressure.dart';
import 'package:nice_button/nice_button.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BPForm extends StatelessWidget {

     setSys(String sys) async {
          final prefs = await SharedPreferences.getInstance();
          prefs.setString("sys", sys);
          print('$sys was saved to file');
     }
     setDys(String dys) async {
          final prefs = await SharedPreferences.getInstance();
          prefs.setString("dys", dys);
          print('$dys was saved to file');
     }
     Color firstColor = Colors.red;
     var sysval = new TextEditingController();
     var dysval = new TextEditingController();

     @override
     Widget build(BuildContext context) {
          return Scaffold(
               appBar: AppBar(
                    title: Text("Dashboard",
                        style: TextStyle(color: Colors.white)
                    ),
                    backgroundColor: Colors.red,
               ),
               body:Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                         new Padding( padding: EdgeInsets.all(20.0)),
                         new TextField(
                              decoration: const InputDecoration(
                                   hintText: 'Enter Systolic reading (upper #)',
                              ),
                              controller: sysval,
                              maxLength: 3,
                              keyboardType: TextInputType.number,
                         ),
                         new Padding( padding: EdgeInsets.all(20.0)),
                         new TextField(
                              decoration: const InputDecoration(
                                   hintText: 'Enter Diastolic reading (lower #)',
                              ),
                              controller: dysval,
                              maxLength: 3,
                              keyboardType: TextInputType.number,
                         ),
                         new Padding( padding: EdgeInsets.all(20.0)),
                         new NiceButton(
                              text: "Update",
                              background: firstColor,
                              width: 150,
                              radius: 25,
                              onPressed: () {
                                   saveResults();
                                   Navigator.push(
                                        context,
                                        MaterialPageRoute(builder: (context) => Dashboard()),
                                   );
                              },
                         )
                    ],

               ),
          );
     }
     void saveResults() {
          var sys = sysval.text;
          setSys(sys);
          var dys = dysval.text;
          setDys(dys);
     }
}
