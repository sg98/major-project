import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nice_button/nice_button.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'cards.dart';

Color firstColor = Colors.red;

class Lessons extends StatelessWidget {
     @override
     Widget build(BuildContext context) {
          return new MaterialApp(
               title: "Lessons",
               home: new Lesson(title: 'Lessons'),
          );
     }
}
class Lesson extends StatefulWidget {
     Lesson({Key key, this.title}) : super(key: key);

     final String title;

     @override
     LessonPage createState() => new LessonPage();
}

class LessonPage extends State<Lesson>{
     int score ;
     bool _isPressed = false;

     getScore() async {
          final prefs = await SharedPreferences.getInstance();
          int scoreExist = prefs.getInt("score");
          print("$scoreExist is stored in sp");
          if(scoreExist != null ){
               setState(() {
                 this.score = scoreExist;
               });
               int scoreUpdate = prefs.getInt("score");
               if (scoreUpdate < 15 && _isPressed == true){
                    scoreUpdate = scoreUpdate + 3 ;
                    prefs.setInt("score", scoreUpdate);
                    int newScore = prefs.getInt("score");
                    setState(() {
                      this.score = newScore;
                    });
               }else if(scoreUpdate >= 15){
                    scoreUpdate = scoreUpdate + 0;
                    setState(() {
                      this.score = 15;
                    });
               }else{
                    scoreUpdate = scoreUpdate + 0;
               }
          }else if (scoreExist == null){
               prefs.setInt("score", 0);
               int setScore = prefs.getInt("score");
               setState(() {
                 this.score = setScore;
               });
          }
     }
     @override
     void initState(){
          getScore();
          super.initState();
     }

     Widget build(BuildContext context) {
          return Scaffold(
               appBar: new AppBar(
                    title: new Text("Lessons",
                         style: TextStyle(color: Colors.white)
                    ),
                    backgroundColor: Colors.red,
               ),
               body:
               ListView(
                    children: <Widget>[
                         new Text("Score: $score",
                             style: new TextStyle(fontSize: 40.0, color: Colors.red[900]),),
                         new Padding(padding: EdgeInsets.all(10)),
                         Card1(),
                        NiceButton(
                             background: firstColor,
                              text: 'Complete Lesson 1',
                              icon: Icons.check,
                              radius: 52.0,
                              onPressed: () {
                                   setState(() {
                                        this._isPressed = true;
                                   });
                                   getScore();
                              },
                         ),
                         new Padding(padding: EdgeInsets.fromLTRB( 5, 5, 5, 5)),
                         Card2(),
                         NiceButton(
                              background: firstColor,
                              text: 'Complete Lesson 2',
                              icon: Icons.check,
                              radius: 52.0,
                              onPressed: () {
                                   setState(() {
                                        this._isPressed = true;
                                   });
                                   getScore();
                              },
                         ),
                         new Padding(padding: EdgeInsets.fromLTRB( 5, 5, 5, 5)),
                         Card3(),
                         NiceButton(
                              background: firstColor,
                              text: 'Complete Lesson 3',
                              icon: Icons.check,
                              radius: 52.0,
                              onPressed: () {
                                   setState(() {
                                        this._isPressed = true;
                                   });
                                   getScore();
                              },
                         ),
                         new Padding(padding: EdgeInsets.fromLTRB( 5, 5, 5, 5)),
                         Card4(),
                         NiceButton(
                              background: firstColor,
                              text: 'Complete Lesson 4',
                              icon: Icons.check,
                              radius: 52.0,
                              onPressed: () {
                                   setState(() {
                                        this._isPressed = true;
                                   });
                                   getScore();
                              },
                         ),
                         new Padding(padding: EdgeInsets.fromLTRB( 5, 5, 5, 5)),
                         Card5(),
                         NiceButton(
                              background: firstColor,
                              text: 'Complete Lesson 5',
                              icon: Icons.check,
                              radius: 52.0,
                              onPressed: () {
                                   setState(() {
                                        this._isPressed = true;
                                   });
                                   getScore();
                              },
                         ),
                         new Padding(padding: EdgeInsets.fromLTRB( 5, 5, 5, 5)),
                    ].map((w) {
                         return Padding(
                              padding: EdgeInsets.only(top: 15, left: 15, right: 15,),
                              child: w,
                         );

                    }).toList(),
               ),
               backgroundColor: Colors.white,
          );
     }
}
