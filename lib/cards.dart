import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:expandable/expandable.dart';

//first Lesson paragraphs
const p11 = "\nHypertension, more popularly known as high blood pressure, occurs when the force of your blood pushing against the walls of your blood vessels, is consistently too high. The more blood your heart pumps and the narrower your arteries, the higher your blood pressure.\n";
const p12 = "\nYou can have hypertension for years without any symptoms. As such hypertension is often referred to as the “silent killer”. Even without symptoms, damage to blood vessels and your heart continues and can be detected.\n";
const p13 = "\nBlood pressure is written as two numbers. The first (systolic) number represents the pressure in blood vessels when the heart contracts or beats. The second (diastolic) number represents the pressure in the vessels when the heart rests between beats.\n\n Normal blood pressure is less than 120 over 80 (120/80), while high blood pressure is 140 over 90 (140/90) or higher.\n";

//Lesson 2 paragraphs
const p21 = "\n	Jamaica\n\n"
    "•	Approximately 1 in 4 Jamaicans (25%) suffer from hypertension.\n"
    "•	50% of Jamaicans have a parent or grandparent with hypertension.\n"
    "•	Approximately 50% of Jamaicans with hypertension do not know they have the condition.\n"
    "•	The prevalence of hypertension is much higher for persons who have diabetes than those who do not have diabetes.\n"
    "•	Jamaican females are at higher risk to have hypertension because of higher levels of obesity and high cholesterol.\n"
    "•	Most Jamaicans affected by high blood pressure are those in the lower to middle income bracket.\n"
    "\n";
const p22 = "\n	Worldwide\n\n"
    "According to the World Health Organization (WHO)\n"
    "•	An estimated 1.13 billion people worldwide have hypertension.\n"
    "•	In 2015, 1 in 4 men and 1 in 5 women had hypertension.\n"
    "•	Less than 1 in 5 people with hypertension have the problem under control.\n"
    "•	Hypertension is a major cause of early death worldwide.\n"
    "•	The risk of hypertension increases with age and is higher among black people than other races\n\n.";


//Lesson 3 Paragraphs
const  p3 = "\nGenetics – Family health history is an important factor to note, as certain traits can be heredity meaning passing down through genes. Individuals with a family history of hypertension that share common environments and lifestyles have an increased risk of hypertension\n"
    "\nSmoking (Tobacco) – Smoking increases the risk of hypertension, because it assists in the damaging of the heart and blood vessels, Nicotine a key ingredient in cigarettes also raises blood pressure.\n"
    "\nAlcohol Use – An excessive use of alcohol raises blood pressure and can contribute to the development of hypertension.\n"
    "\nObesity – Obesity represents having excess body fat, being overweight. Being obese puts more pressure on the heart to go pump blood around the body which adds increased stress on the heart and blood vessels.\n"
    "\nUnhealthy Diet – Eating healthy is key to staying healthy. Diets high in sodium and low in potassium leaves one at high risk for Hypertension.\n"
    "\nPhysical Inactivity – Being physically active helps heart and blood vessels stay healthy, in turn reducing risk of developing hypertension.\n"
    "\nAge – The likeliness of developing hypertension increases with age, but does not mean a young person cannot develop hypertension.\n"
    "\nEthnicity – Statistically black persons are more likely to develop hypertension compared to other races and have a higher chance of developing it earlier in life.\n"
    "\n";

//Lesson 4 paragraphs
const p41 = "\nTo help manage your blood pressure, you should limit the amount of salt that you eat and increase the amount of potassium in your diet. It is also important to eat foods that are lower in fat, as well as plenty of fruits, vegetables, and whole grains. The DASH diet is an example of an eating plan that can help you to lower your blood pressure.";
const p42 = "\nGetting regular exercise. Exercise can help you maintain a healthy weight and lower your blood pressure. You should try to get moderate-intensity aerobic exercise at least 2 and a half hours per week, or vigorous-intensity aerobic exercise for 1 hour and 15 minutes per week. Aerobic exercise, such as brisk walking, is any exercise in which your heart beats harder and you use more oxygen than usual.\n";
const p43 = "\nBeing at a healthy weight. Being overweight or having obesity increases your risk for high blood pressure. Maintaining a healthy weight can help you control high blood pressure and reduce your risk for other health problems.\n";
const p44 = "\nLimiting alcohol. Drinking too much alcohol can raise your blood pressure. It also adds extra calories, which may cause weight gain. Men should have no more than two drinks per day, and women only one. \n";
const p45 = "\nNot smoking. Cigarette smoking raises your blood pressure and puts you at higher risk for heart attack and stroke. If you do not smoke, do not start. If you do smoke, talk to your health care provider for help in finding the best way for you to quit.\n";
const p46 = "\nManaging stress. Learning how to relax and manage stress can improve your emotional and physical health and lower high blood pressure. Stress management techniques include exercising, listening to music, focusing on something calm or peaceful, and meditating.\n";

//Lesson 5 paragraphs
const p51 = "\nThe strain and pressure that hypertension put on blood vessels and arteries in turn affect the rest of the body detrimentally.\n";
const p52 = "\nKidney Disease/Failure – The kidneys serve as a filter system to the body that removes waste products and excess fluid. Having a high blood pressure causes damage to arteries around the kidneys over time, making them weaker, harder or narrower, which in turn makes it harder or impossible for them to deliver blood to the kidney tissue.\n"
    "\nHeart Failure/attacks – Heart failure occurs when the heart is unable to provide the body with enough blood. The damage to arteries and blood vessels caused by hypertension causing them to become narrow or blockages in the vessels increase the chances of heart failure or in turn cause heart attacks.\n"
    "\nStroke – The strain hypertension puts on blood vessels can create clots making the vessels narrower and harder, when these clots develop in or reach the brain, a stroke can occur or a brain aneurysm.\n"
    "\nVision Loss – Hypertension can cause damage to blood vessels connected to the eyes, and damage to these vessels can damage the retina and even cause bleeding in the eye\n\n";

class Card1 extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Card(
      clipBehavior: Clip.antiAlias,
      child: ExpandableNotifier(
        child: Column(
          children: <Widget>[
              SizedBox(
              height: 150.0,
              child: Container(
                decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      image: DecorationImage(
                      alignment: Alignment.center,
                      matchTextDirection: true,
                      repeat: ImageRepeat.noRepeat,
                      image: AssetImage('assets/l13.jpg'),
                    )
                ),
                ),
              ),
            ExpandablePanel(
              tapHeaderToExpand: true,
              headerAlignment: ExpandablePanelHeaderAlignment.center,
              header: Padding(
                padding: EdgeInsets.all(10),
                child: Text("What is hypertension?",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                )
              ),
              collapsed: Text(p11, softWrap: false, overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontSize: 20)),
              expanded: Column(children: <Widget>[
                   Text(p11, softWrap: true, overflow: TextOverflow.fade,
                   style: TextStyle(fontSize: 20)),
                   Image(image: AssetImage('assets/l11.jpg')),
                   Text(p12, softWrap: true, overflow: TextOverflow.fade,
                       style: TextStyle(fontSize: 20)),
                   Image(image: AssetImage('assets/l12.jpg')),
                   Text(p13, softWrap: true, overflow: TextOverflow.fade,
                       style: TextStyle(fontSize: 20)),
                   Image(image: AssetImage('assets/l13.jpg')),
              ]),
              builder: (_, collapsed, expanded) {
                return Padding(
                  padding: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 10.0),
                  child: Expandable(
                    collapsed: collapsed,
                    expanded: expanded,
                  ),
                );
              },
            ),

          ],
        ),
      )
    );
  }
}

class Card2 extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Card(
      clipBehavior: Clip.antiAlias,
      child: ExpandableNotifier(
        child: Column(
          children: <Widget>[
              SizedBox(
              height: 150.0,
              child: Container(
                   decoration: BoxDecoration(
                       shape: BoxShape.rectangle,
                       image: DecorationImage(
                            alignment: Alignment.center,
                            matchTextDirection: true,
                            repeat: ImageRepeat.noRepeat,
                            image: AssetImage('assets/l21.jpg'),
                       )
                   ),
              ),
            ),
            ExpandablePanel(
              tapHeaderToExpand: true,
              headerAlignment: ExpandablePanelHeaderAlignment.center,
              header: Padding(
                padding: EdgeInsets.all(10),
                child: Text("Hypertension Statistics",
                     style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                )
              ),
              collapsed: Text(p21, softWrap: false, overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontSize: 20)),
              expanded: Column(children: <Widget>[
                   Text(p21, softWrap: true, overflow: TextOverflow.fade,
                       style: TextStyle(fontSize: 20)),
                   Image(image: AssetImage('assets/l21.jpg')),
                   Text(p22, softWrap: true, overflow: TextOverflow.fade,
                       style: TextStyle(fontSize: 20)),
                   Image(image: AssetImage('assets/l22.jpg')),
              ]),
              builder: (_, collapsed, expanded) {
                return Padding(
                  padding: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 10.0),
                  child: Expandable(
                    collapsed: collapsed,
                    expanded: expanded,
                  ),
                );
              },
            ),
          ],
        ),
      )
    );
  }
}

class Card3 extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Card(
      clipBehavior: Clip.antiAlias,
      child: ExpandableNotifier(
        child: Column(
          children: <Widget>[
              SizedBox(
              height: 150.0,
              child: Container(
                decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    image: DecorationImage(
                         alignment: Alignment.center,
                         matchTextDirection: true,
                         repeat: ImageRepeat.noRepeat,
                         image: AssetImage('assets/l3.jpg'),
                    )
                ),
              ),
            ),
            ExpandablePanel(
              tapHeaderToExpand: true,
              headerAlignment: ExpandablePanelHeaderAlignment.center,
              header: Padding(
                padding: EdgeInsets.all(10),
                child: Text("Causes of hypertension",
                     style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                )
              ),
              collapsed: Text(p3, softWrap: false, overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontSize: 20)),
              expanded: Column(children: <Widget>[
                   Image(image: AssetImage('assets/l3.jpg')),
                   Text(p3, softWrap: true, overflow: TextOverflow.fade,
                       style: TextStyle(fontSize: 20)),
              ]),
              builder: (_, collapsed, expanded) {
                return Padding(
                  padding: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 10.0),
                  child: Expandable(
                    collapsed: collapsed,
                    expanded: expanded,
                  ),
                );
              },
            ),
          ],
        ),
      )
    );
  }
}

class Card4 extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Card(
      clipBehavior: Clip.antiAlias,
      child: ExpandableNotifier(
        child: Column(
          children: <Widget>[
              SizedBox(
              height: 150.0,
              child: Container(
                decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    image: DecorationImage(
                         alignment: Alignment.center,
                         matchTextDirection: true,
                         repeat: ImageRepeat.noRepeat,
                         image: AssetImage('assets/l41.jpg'),
                    )
                ),
              ),
            ),
            ExpandablePanel(
              tapHeaderToExpand: true,
              headerAlignment: ExpandablePanelHeaderAlignment.center,
              header: Padding(
                padding: EdgeInsets.all(10),
                child: Text("Prevention Methods",
                     style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                )
              ),
              collapsed: Text(p41, softWrap: false, overflow: TextOverflow.ellipsis,
                   style: TextStyle(fontSize: 20)),
              expanded: Column(children: <Widget>[
                   Text(p41, softWrap: true, overflow: TextOverflow.fade,
                       style: TextStyle(fontSize: 20)),
                   Image(image: AssetImage('assets/l41.jpg')),
                   Text(p42, softWrap: true, overflow: TextOverflow.fade,
                   style: TextStyle(fontSize: 20)),
                   Image(image: AssetImage('assets/l42.jpg')),
                   Text(p43, softWrap: true, overflow: TextOverflow.fade,
                   style: TextStyle(fontSize: 20)),
                   Image(image: AssetImage('assets/l43.jpg')),
                   Text(p44, softWrap: true, overflow: TextOverflow.fade,
                   style: TextStyle(fontSize: 20)),
                   Image(image: AssetImage('assets/l44.png')),
                   Text(p45, softWrap: true, overflow: TextOverflow.fade,
                   style: TextStyle(fontSize: 20)),
                   Image(image: AssetImage('assets/l45.png')),
                   Text(p46, softWrap: true, overflow: TextOverflow.fade,
                   style: TextStyle(fontSize: 20)),
                   Image(image: AssetImage('assets/l46.jpg')),
              ]),
              builder: (_, collapsed, expanded) {
                return Padding(
                  padding: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 10.0),
                  child: Expandable(
                    collapsed: collapsed,
                    expanded: expanded,
                  ),
                );
              },
            ),
          ],
        ),
      )
    );
  }
}

class Card5 extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Card(
      clipBehavior: Clip.antiAlias,
      child: ExpandableNotifier(
        child: Column(
          children: <Widget>[
              SizedBox(
              height: 150.0,
              child: Container(
                decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    image: DecorationImage(
                         alignment: Alignment.center,
                         matchTextDirection: true,
                         repeat: ImageRepeat.noRepeat,
                         image: AssetImage('assets/l5.png'),
                    )
                ),
              ),
            ),
            ExpandablePanel(
              tapHeaderToExpand: true,
              headerAlignment: ExpandablePanelHeaderAlignment.center,
              header: Padding(
                padding: EdgeInsets.all(10),
                child: Text("Effects of Hypertension",
                     style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                )
              ),
              collapsed: Text(p51, softWrap: false, overflow: TextOverflow.ellipsis,
                   style: TextStyle(fontSize: 20)),
              expanded: Column(children: <Widget>[
                   Text(p51, softWrap: true, overflow: TextOverflow.fade,
                   style: TextStyle(fontSize: 20)),
                   Image(image: AssetImage('assets/l5.png')),
                   Text(p52, softWrap: true, overflow: TextOverflow.fade,
                   style: TextStyle(fontSize: 20)),
              ]),
              builder: (_, collapsed, expanded) {
                return Padding(
                  padding: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 10.0),
                  child: Expandable(
                    collapsed: collapsed,
                    expanded: expanded,
                  ),
                );
              },
            ),
          ],
        ),
      )
    );
  }
}


